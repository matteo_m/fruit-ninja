$(function(){
////////////////////////////////////////////////////////////
////// GLOBAL
/////////////////////////////////////////////////////////
var  board     = {}
	,splitElem = {}
	,trace     = {}
	;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// BOARD FUNCTIONS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
board.init = function() {
	b = this;
	this.id ="#canvas";
	this.sections = new Array();
	this.mousedown = false;
	this.elems = new Array();
	this.nCol = 12;  //num max column of grid
	this.grid = new Array();
}
board.set = function(){
	this.buildGrid();
	this.elems.push(splitElem.init("#canvas","body"));
}
board.interaction = function(){
	// 	var that = this;
}
board.buildGrid = function(){
	this.grid.push(0)
	for(var i = 1; i < this.nCol+1;i++){
		var $el = $('<div class="row-fluid"><div class="span'+i+'"></div></div>').find(".span"+i);
		width = $el.appendTo('body').width();
		$el.remove();
		//console.log($el.attr("class")+" W: "+width);
		this.grid.push(width);
	}
}
board.gridMatch = function(x){
	for(i =1; i < this.nCol+1; i++){
		if(x < this.grid[i])
			return {c1:parseInt(i),c2:parseInt(this.nCol-i)}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// SPLIT ELEM FUNCTION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
splitElem.init = function(id,parent){
	this.id = id;
	console.log("init splitElem: "+this.id);
	this.selected = false;
	this.parent = parent;
	this.mousedown = false;
	this.trace = null;
	this.elems = Array();
	this.set();
}
splitElem.set = function(){
	this.interaction();
}
splitElem.interaction =  function(){
	var that = this;
	console.log("interaction on: "+that.id);
	
	//move inside cut
	/////////////////////////////////////////////////////////////////////////////
	$(this.id).bind('mousedown', function(event){
		event.stopPropagation(); //select only element on top
		that.mousedown = true;	
		that.trace = trace.init();
		console.log("----Mousedown on "+that.id);
		that.trace.start_x = event.clientX;
		that.trace.start_y  = event.clientY;
		$(that.id).bind('mousemove', function(event){

			if(that.mousedown){

				//console.log("mousemove");
				//console.log("clientX/Y: " + event.clientX + ", " +event.clientY);
			}
		})
	});
	$(this.id).bind('mouseup', function(){
		//console.log("mouseup");
		that.mousedown = false;				
		that.trace.end_x = event.clientX;
		that.trace.end_y  = event.clientY;
		console.log(that.trace.get());
		that.split();
	});
	//////////////////////////////////////////////////////////////////////////////
	// $(this.id).click( function(event){
	// 	that.selected = true;
	// 	console.log("click on "+that.id);
	// });
}
splitElem.split = function(){
	var html = "";
	var cl = "";
	var dir = this.trace.direction();
		
	if( dir == "vert"){
		console.log("vert");
		cl = board.gridMatch(Math.abs(trace.start_x+trace.end_x)/2);
		this.createSpan(cl);
	}
	else if(dir == "horiz"){
	 	console.log("horiz");
	 	this.createRow();
	// 		cl ="row-fluid";
	// 		html='<div class='+cl+'><div class="span span12"></div></div><div class='+cl+'><div class="span span12"></div></div>';
	}
	else{
		console.log("error");
	}
	// 	$(this.id).append(html);
}
splitElem.createRow = function(){

	this.elems.push({
		id: ".span"+board.nCol,
		col: board.nCol
	});
	//remove old span
	this.createHtml();

}
splitElem.createSpan = function(cl){
	console.log("new span: "+cl.c1+" "+cl.c2);
	if(this.elems.length >= 2){
		var dim = 0
		var copy = new Array();
		var c = null;
		for(i = 0 ; i < this.elems.length; i++){
			dim += this.elems[i].col; 
			//console.log("i :"+i+" dim:"+dim+" col:"+this.elems[i].col);
			if( cl.c1 > dim && c == null) {    //split this col
				copy.push(this.elems[i].col)
				//console.log("push "+copy[copy.length-1]+" in ["+(copy.length-1)+"]")
			}
			else if(cl.c1 < dim && c == null){
				copy.push(cl.c1 - Math.abs(dim - this.elems[i].col));
				//console.log("push "+copy[copy.length-1]+" in ["+(copy.length-1)+"]")
				copy.push( Math.abs( this.elems[i].col - copy[copy.length-1]));
				//console.log("push "+copy[copy.length-1]+" in ["+(copy.length-1)+"]")
				c = i+1;
			}
		}
		
		for( i = c ; i < this.elems.length; i++ ){
			copy.push(this.elems[i].col)
		}

		
		//remove old span
		this.clearElems(); 
		this.elems = new Array();
		//add new span
		for(i in copy){
			//console.log(copy[i]);
			this.elems.push({
				class: ".span"+copy[i],
				col: copy[i]
			});
		}
	}
	else{
		this.elems.push({
			class: ".span"+cl.c1,
			col: cl.c1
		});
		this.elems.push({
			class: ".span"+cl.c2,
			col: cl.c2
		});
	}
	this.createHtml();
	this.get();
}
splitElem.createHtml = function(){
	this.clearElems();
	this.addElems();
	this.setRowHeight();
}
splitElem.get = function(){
	for(i in this.elems){
		console.log(this.elems[i])
	}
}
splitElem.clearElems = function(){
	$(this.id +" .row-fluid").remove();
	// for(i in this.elems){
		
	// 	console.log("remove "+this.elems[i].id);
	// }
}
splitElem.setRowHeight = function(){
	var h = 100 / $(this.id).children(".row-fluid").length; 
	$(this.id+" .row-fluid .span").each(function(){
		$(this).height(h+"%");
	})
}
splitElem.addElems = function(){
	var html="";
	var dim = 0;


	for(i in this.elems){
		//console.log(this.id+" split in span:"+cl.c1+" and span:"+cl.c2);
		
		if(dim == 0 ){
			html+='<div class="row-fluid">'
		}
		html+='<div class="span span'+this.elems[i].col+'"></div>';
		dim += this.elems[i].col;
		if(dim >= board.nCol ){
			html+='</div>';
			dim = 0;
		}
		console.log("add "+this.elems[i].class);
	}

	html+='</div>'; //end row
	$(this.id).append(html);
}
splitElem.update = function(){
	if(this.selected){
		$(this.id).addClass("selected");
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// TRACE
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
trace.init = function(){
	this.start_x  = 0;
	this.start_y  = 0;
	this.end_x    = 0;
	this.end_y    = 0;
	return this;
}
trace.get = function(){
	return{
		start_x  : this.start_x,
		start_y  : this.start_y,
		end_x    : this.end_x,
		end_y    : this.end_y
	}
}
trace.direction =  function(){
	var X = Math.abs( this.start_x - this.end_x);
	var Y = Math.abs( this.start_y - this.end_y);
	if(Y > X)
		return "vert";
	else
		return "horiz";
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// COMMON FUNCTIONS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// START 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
board.init();
board.set();

});

